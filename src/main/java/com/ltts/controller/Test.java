package com.ltts.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/")
public class Test {

	@GetMapping("test")
	public ResponseEntity<String> getTest() {
		String response = "Hello from Sample Springboot Application!";
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}